/**
 * Copyright 2013 Ricardo Padilha
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Ricardo Padilha - ricardo.padilha@usi.ch
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.retwis;

import java.util.NavigableSet;

public interface RetwisClientInterface {
   
   Long register(String userNamed);
   void post(Long uid, String post);
   void follow(Long followerId, Long followedId);
   void unfollow(Long followerId, Long followedId);
   void follow_NOP(Long followerId, Long followedId);
   void unfollow_NOP(Long followerId, Long followedId);
   NavigableSet<RetwisPost> getTimeline(Long uid);
}
