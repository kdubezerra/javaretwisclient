/**
 * Copyright 2013 Ricardo Padilha
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Ricardo Padilha - ricardo.padilha@usi.ch
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.retwis;

import com.google.common.base.Objects;

public class RetwisPost implements Comparable<RetwisPost> {

   public final long timestamp;
   public final String user;
   public final String message;

   public RetwisPost(final long timestamp, final String user, final String message) {
      if (user == null || message == null) {
         if (user == null) System.out.println("user IS NULL");
         if (message == null) System.out.println("message IS NULL");
         throw new NullPointerException();
      }
      this.timestamp = timestamp;
      this.user = user;
      this.message = message;
   }

   @Override
   public int compareTo(final RetwisPost o) {
      return (int) (timestamp - (o.timestamp));
   }

   @Override
   public String toString() {
      return Objects.toStringHelper(this).add("user", user).add("message", message).add("time", Long.toString(timestamp)).toString();
   }
}