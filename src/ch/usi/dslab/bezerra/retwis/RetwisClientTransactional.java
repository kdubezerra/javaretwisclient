/**
 * Copyright 2013 Ricardo Padilha
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Ricardo Padilha - ricardo.padilha@usi.ch
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.retwis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Response;
import redis.clients.jedis.Transaction;

import com.google.common.base.Function;
import com.google.common.collect.Maps;

public class RetwisClientTransactional implements RetwisClientInterface {
   
   private JedisPool jedisPool; 

   
   
   
   public RetwisClientTransactional(String redisServerHost, int redisServerPort) {
      jedisPool = new JedisPool(new JedisPoolConfig(), redisServerHost, redisServerPort);
   }

   
   
   
   @Override
   public Long register(final String user) {
      Jedis client = jedisPool.getResource();
      
      final Response<Long> ruid;
      final Transaction tx1 = client.multi();
         ruid = tx1.incr("global:nextUserId");
      tx1.exec();

      final Long uid = ruid.get();

      final Transaction tx2 = client.multi();
         tx2.set("username:" + user + ":id"       , uid.toString());
         tx2.set("uid:"      + uid   + ":username", user);
      tx2.exec();
      
      jedisPool.returnResource(client);
      return uid;
   }

   
   
   
   public Map<String, Long> register(final String... users) {
      Jedis client = jedisPool.getResource();
      
      final ArrayList<Response<Long>> uids = new ArrayList<>(users.length);
      final Transaction tx1 = client.multi();
      for (int i = 0; i < users.length; i++) {
         uids.add(tx1.incr("global:nextUserId"));
      }
      tx1.exec();

      final Map<String, Long> ids = new HashMap<>();

      final Transaction tx2 = client.multi();
         for (int i = 0; i < users.length; i++) {
            final String user = users[i];
            final Long uid = uids.get(i).get();
            tx2.set("username:" + user + ":id"       , uid.toString());
            tx2.set("uid:"      + uid   + ":username", user);
            ids.put(user, uid);
         }
      tx2.exec();

      jedisPool.returnResource(client);
      return ids;
   }

   
   
   
   public String lookup(final Long user) {
      Jedis client = jedisPool.getResource();
      
      final Transaction tx1 = client.multi();
         final Response<String> name = tx1.get("uid:" + user + ":username");
      tx1.exec();
      return name.get();
   }

   
   
   
   public Map<Long, String> lookup(final Long... users) {
      Jedis client = jedisPool.getResource();
      
      final Map<Long, Response<String>> map = new HashMap<>();

      final Transaction tx1 = client.multi();
         for (final Long user : users) {
            map.put(user, tx1.get("uid:" + user + ":username"));
         }
      tx1.exec();

      jedisPool.returnResource(client);
      
      return Maps.transformValues(map, new Function<Response<String>, String>() {
         @Override public String apply(final Response<String> r) {
            return r.get();
         }
      });
   }
   
   
   
   
   @Override
   public void post(final Long user, final String message) {
      Jedis client = jedisPool.getResource();
      
      final Response<Long> pid;
      final Response<Set<String>> followers;

      final Transaction tx1 = client.multi();
         pid = tx1.incr("global:nextPostId");
         followers = tx1.smembers("uid:" + user + ":followers");
      tx1.exec();

      final Transaction tx2 = client.multi();
         tx2.set("post:" + pid, user + "|" + System.currentTimeMillis() + "|" + message);
         tx2.lpush("uid:" + user + ":posts", pid.toString());
         for (final String follower : followers.get()) {
            tx2.lpush("uid:" + follower + ":posts", pid.toString());
         }
      tx2.exec();
      
      jedisPool.returnResource(client);
   }

   
   
   
   public void post(final Long user, final String... messages) {
      Jedis client = jedisPool.getResource();
      
      final ArrayList<Response<Long>> pids = new ArrayList<>(messages.length);
      final Response<Set<String>> followers;

      final Transaction tx1 = client.multi();
         for (int i = 0; i < messages.length; i++) {
            pids.add(tx1.incr("global:nextPostId"));
         }
         followers = tx1.smembers("uid:" + user + ":followers");
      tx1.exec();

      final Transaction tx2 = client.multi();
         for (int i = 0; i < messages.length; i++) {
            final Long pid = pids.get(i).get();
            final String message = messages[i];
            tx2.set("post:" + pid, user + "|" + System.currentTimeMillis() + "|" + message);
            tx2.lpush("uid:" + user + ":posts", pid.toString());
            for (final String follower : followers.get()) {
               tx2.lpush("uid:" + follower + ":posts", pid.toString());
            }
         }
      tx2.exec();
      
      jedisPool.returnResource(client);
   }

   
   

   @Override
   public void follow(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      
      final Transaction tx1 = client.multi();
         tx1.sadd("uid:" + friend + ":followers", user.toString());
         tx1.sadd("uid:" + user   + ":following", friend.toString());
      tx1.exec();
      
      jedisPool.returnResource(client);
   }
   
   
   
   
   @Override
   public void follow_NOP(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      
      final Transaction tx1 = client.multi();
      tx1.sadd("uid:" + friend + ":followers_NOP", user.toString());
      tx1.sadd("uid:" + user   + ":following_NOP", friend.toString());
      tx1.exec();
      
      jedisPool.returnResource(client);
   }

   
   
   
   public void follow(final Long user, final Long... friends) {
      Jedis client = jedisPool.getResource();
      
      final Transaction tx1 = client.multi();
         for (final Long friend : friends) {
            tx1.sadd("uid:" + friend + ":followers", user.toString());
            tx1.sadd("uid:" + user   + ":following", friend.toString());
         }
      tx1.exec();
      
      jedisPool.returnResource(client);
   }

   
   
   
   @Override
   public void unfollow(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      
      final Transaction tx1 = client.multi();
         tx1.srem("uid:" + friend + ":followers", user.toString());
         tx1.srem("uid:" + user   + ":following", friend.toString());
      tx1.exec();
      
      jedisPool.returnResource(client);
   }
   
   
   
   
   @Override
   public void unfollow_NOP(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      
      final Transaction tx1 = client.multi();
         tx1.srem("uid:" + friend + ":followers_NOP", user.toString());
         tx1.srem("uid:" + user   + ":following_NOP", friend.toString());
      tx1.exec();
      
      jedisPool.returnResource(client);
   }

   
   
   
   public void unfollow(final Long user, final Long... friends) {
      Jedis client = jedisPool.getResource();
      
      final Transaction tx1 = client.multi();
         for (final Long friend : friends) {
            tx1.srem("uid:" + friend + ":followers", user.toString());
            tx1.srem("uid:" + user   + ":following", friend.toString());
         }
      tx1.exec();
      
      jedisPool.returnResource(client);
   }

   
   
   
   public Map<Long, String> getFollowers(final Long user) {
      Jedis client = jedisPool.getResource();
      
      final Map<Long, Response<String>> map = new HashMap<>();

      final Response<Set<String>> followers;
      final Transaction tx1 = client.multi();
         followers = tx1.smembers("uid:" + user + ":followers");
      tx1.exec();

      final Transaction tx2 = client.multi();
         for (final String follower : followers.get()) {
            map.put(Long.valueOf(follower), tx2.get("uid:" + follower + ":username"));
         }
      tx2.exec();

      jedisPool.returnResource(client);
      
      return Maps.transformValues(map, new Function<Response<String>, String>() {
         @Override public String apply(final Response<String> r) {
            return r.get();
         }
      });
   }

   
   
   @Override
   public NavigableSet<RetwisPost> getTimeline(final Long user) {
      Jedis client = jedisPool.getResource();
      
      final TreeSet<RetwisPost> timeline = new TreeSet<>();

      final Response<List<String>> postIds;
      final Transaction tx1 = client.multi();
         postIds = tx1.lrange("uid:" + user + ":posts", 0, 10);
      tx1.exec();
      final int length = postIds.get().size();

      final ArrayList<Response<String>> posts = new ArrayList<>(length);
      final Transaction tx2 = client.multi();
         for (final String postId : postIds.get()) {
            posts.add(tx2.get("post:" + postId));
         }
      tx2.exec();

      final ArrayList<Response<String>> usernames = new ArrayList<>(length);
      final Transaction tx3 = client.multi();
         for (final Response<String> post : posts) {
            final String suid = post.get();
            final String uid = suid.substring(0, suid.indexOf('|'));
            usernames.add(tx3.get("uid:" + uid + ":username"));
         }
      tx3.exec();

      for (int i = 0; i < length; i++) {
         final String post = posts.get(i).get();
         final StringTokenizer st = new StringTokenizer(post, "|");
         st.nextToken(); // discard first token
         final String name = usernames.get(i).get();
         final long time = Long.parseLong(st.nextToken());
         final String message = st.nextToken();
         timeline.add(new RetwisPost(time, name, message));
      }
      
      jedisPool.returnResource(client);
      return timeline;
   }
   
   
   
   
}
