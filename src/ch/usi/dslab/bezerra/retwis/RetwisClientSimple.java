/**
 * Copyright 2013 Ricardo Padilha
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Ricardo Padilha - ricardo.padilha@usi.ch
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.retwis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RetwisClientSimple implements RetwisClientInterface {

   private JedisPool jedisPool; 

   public RetwisClientSimple(String redisServerHost, int redisServerPort) {
      jedisPool = new JedisPool(new JedisPoolConfig(), redisServerHost, redisServerPort);
   }

   @Override
   public Long register(final String user) {
      Jedis client = jedisPool.getResource();
      final Long uid = client.incr("global:nextUserId");
      client.set("username:" + user + ":id", uid.toString());
      client.set("uid:" + uid + ":username", user);
      jedisPool.returnResource(client);
      return uid;
   }
   
   public Map<String, Long> register(final String... users) {
      final Map<String, Long> ids = new HashMap<>();
      for (final String user : users) {
         ids.put(user, register(user));
      }
      return ids;
   }
   
   public String lookup(final Long user) {
      Jedis client = jedisPool.getResource();
      final String name = client.get("uid:" + user + ":username");
      jedisPool.returnResource(client);
      return name;
   }

   public Map<Long, String> lookup(final Long... users) {
      final Map<Long, String> names = new HashMap<>();
      Jedis client = jedisPool.getResource();
      for (final Long user : users) {
         String name = client.get("uid:" + user + ":username");
         names.put(user, name);
      }
      jedisPool.returnResource(client);
      return names;
   }

   @Override
   public void post(final Long user, final String message) {
      Jedis client = jedisPool.getResource();
      final String pid = client.incr("global:nextPostId").toString();
      client.set("post:" + pid, user + "|" + System.currentTimeMillis() + "|" + message);
      // update followers synchronously
      client.lpush("uid:" + user + ":posts", pid);
      final Set<String> followers = client.smembers("uid:" + user + ":followers");
      for (String follower : followers) {
         Long fid = Long.valueOf(follower);
         client.lpush("uid:" + fid + ":posts", pid);
      }
      jedisPool.returnResource(client);
   }

   public void post(final Long user, final String... messages) {
      for (final String message : messages) {
         post(user, message);
      }
   }

   @Override
   public void follow(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      client.sadd("uid:" + friend + ":followers", user.toString());
      client.sadd("uid:" + user   + ":following", friend.toString());
      jedisPool.returnResource(client);
   }
   
   @Override
   public void follow_NOP(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      client.sadd("uid:" + friend + ":followers_NOP", user.toString());
      client.sadd("uid:" + user   + ":following_NOP", friend.toString());
      jedisPool.returnResource(client);
   }

   public void follow(final Long user, final Long... friends) {
      for (final Long friend : friends) {
         follow(user, friend);
      }
   }

   @Override
   public void unfollow(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      client.srem("uid:" + friend + ":followers", user.toString());
      client.srem("uid:" + user   + ":following", friend.toString());
      jedisPool.returnResource(client);
   }
   
   @Override
   public void unfollow_NOP(final Long user, final Long friend) {
      Jedis client = jedisPool.getResource();
      client.srem("uid:" + friend + ":followers_NOP", user.toString());
      client.srem("uid:" + user   + ":following_NOP", friend.toString());
      jedisPool.returnResource(client);
   }

   public void unfollow(final Long user, final Long... friends) {
      for (final Long friend : friends) {
         unfollow(user, friend);
      }
   }

   public Map<Long, String> getFollowers(final Long user) {
      Jedis client = jedisPool.getResource();
      final Map<Long, String> map = new HashMap<>();
      final Set<String> followers = client.smembers("uid:" + user + ":followers");
      for (final String follower : followers) {
         final Long fid = Long.valueOf(follower);
         final String name = client.get("uid:" + fid + ":username");
         map.put(fid, name);
      }
      jedisPool.returnResource(client);
      return map;
   }

   @Override
   public NavigableSet<RetwisPost> getTimeline(final Long user) {
      Jedis client = jedisPool.getResource();
      final List<String> pids = client.lrange("uid:" + user + ":posts", 0, 1000);
      final TreeSet<RetwisPost> timeline = new TreeSet<>();
      for (final String pid : pids) {
         final String post = client.get("post:" + pid);
         final StringTokenizer st = new StringTokenizer(post, "|");
         final String name = client.get("uid:" + st.nextToken() + ":username");
         final long time = Long.parseLong(st.nextToken());
         final String message = st.nextToken();
         timeline.add(new RetwisPost(time, name, message));
      }
      jedisPool.returnResource(client);
      return timeline;
   }
   
}
